package br.com.itau.itamu.pojo;


//NAO VAI SER USADO
public class OutputContaPojo {

	private long idDivida;
	private String cpfPagador;
	private String cpfRecebedor;
	private String statusEnvio;
	public long getIdDivida() {
		return idDivida;
	}
	public void setIdDivida(long idDivida) {
		this.idDivida = idDivida;
	}
	public String getCpfPagador() {
		return cpfPagador;
	}
	public void setCpfPagador(String cpfPagador) {
		this.cpfPagador = cpfPagador;
	}
	public String getCpfRecebedor() {
		return cpfRecebedor;
	}
	public void setCpfRecebedor(String cpfRecebedor) {
		this.cpfRecebedor = cpfRecebedor;
	}
	public String getStatusEnvio() {
		return statusEnvio;
	}
	public void setStatusEnvio(String statusEnvio) {
		this.statusEnvio = statusEnvio;
	}
	
	
}
