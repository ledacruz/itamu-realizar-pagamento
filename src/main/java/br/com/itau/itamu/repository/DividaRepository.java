package br.com.itau.itamu.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.itamu.models.Divida;

public interface DividaRepository extends CrudRepository<Divida, Long> {

	public Iterable<Divida> listarPagamentosByIdDivida(long IdDivida);

	public void darBaixaDivida();
	
	public void atualizarSaldoConta();

}
