package br.com.itau.itamu.amqConfigures;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import br.com.itau.itamu.pojo.OutputContaPojo;
import br.com.itau.itamu.models.Divida;


//NAO VAI MAIS SER USADO
@Component
public class AMQSender {

	@Autowired
	private JmsMessagingTemplate jmsMessagingTemplate;

	public void sendMessage(Divida divida) {

		OutputContaPojo outputContaPojo = new OutputContaPojo();
		outputContaPojo.setCpfPagador(divida.getCpfPagador());
		outputContaPojo.setCpfRecebedor(divida.getCpfRecebedor());
		outputContaPojo.setIdDivida(divida.getIdDivida());
		outputContaPojo.setStatusEnvio(divida.getStatusEnvio());

		jmsMessagingTemplate.convertAndSend("amq.saida.divida", outputContaPojo);

	}

}
