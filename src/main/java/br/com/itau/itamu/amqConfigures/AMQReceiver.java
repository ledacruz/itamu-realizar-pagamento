package br.com.itau.itamu.amqConfigures;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import br.com.itau.itamu.repository.DividaRepository;
import br.com.itau.itamu.models.Divida;
import br.com.itau.itamu.pojo.InputDividaPojo;

@Component
public class AMQReceiver {

	@Autowired
	DividaRepository dividaRepository;

	@JmsListener(destination = "amq.entrada.divida")
	public void receiveMessage(String msg) {
		InputDividaPojo inputDividaPojo = new Gson().fromJson(msg, InputDividaPojo.class);

		Divida divida = new Divida();

		divida.setCpfPagador(inputDividaPojo.getCpfPagador());
		divida.setCpfRecebedor(inputDividaPojo.getCpfRecebedor());
		divida.setIdDivida(inputDividaPojo.getIdDivida());
		divida.setStatusEnvio(inputDividaPojo.getStatusEnvio());
		dividaRepository.save(divida);

	}

	@JmsListener(destination = "amq.entrada.divida", containerFactory = "pubsub")
	public void listenTopic(String message) {
		System.out.println("New pub <" + message + ">");
	}

}
